from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from courses import views as modules
from django.contrib.auth.models import User
from security import MiddlewarePear as middle
import jwt
import json

@csrf_exempt
def web(request,module,prm,id):
    #credentials = []
    obj=middle
    rol=obj.finderSession()
    inputs = json.loads(request.body)
    token = ('', inputs["tokens"])[inputs["tokens"] != '']
    try:
        #aqui valido el token
        payload = jwt.decode(token, 'SECRET')
        request.user = User.objects.get(
            username=payload.get('username'),
            is_active=True
        )
        print('payloads',payload)
        if module == "course":
            rol.getRolByTokens(token,1)
            return modules.procedureCourse(request,prm,id)
        elif module =="subject":
            rol.getRolByTokens(token,1)
            return modules.procedureSubject(request,prm,id)
        elif module =="cost":
            rol.getRolByTokens(token,4)
            return modules.procedureCost(request,prm,id)
        elif module =="enrolment":
            rol.getRolByTokens(token,2)
            return modules.procedureEnrolment(request,prm,id)
        elif module =="matter":
            rol.getRolByTokens(token,2)
            return modules.procedureMatterContent(request,prm,id)
        elif module =="security":
            rol.getRolByTokens(token,1)
            return modules.procedureSecurity(request,prm,id)
        else:
            print('error: module no found')
    except jwt.ExpiredSignature, jwt.DecodeError:
        return HttpResponse({'Error': "Token is invalid"}, status="403")
    except User.DoesNotExist:
        return HttpResponse({'Error': "Internal server error"}, status="500")

@csrf_exempt
def web1(request,prm,id):
    return modules.procedureCourse1(request,prm,id)