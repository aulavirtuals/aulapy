var app = angular.module("app", ['ngRoute']
    , function ($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
        
    }
    )
        .constant("API_URL", "http://localhost:8000/")
    ;
 app.config(['$httpProvider',function($httpProvider){
 	//$httpProvider.defaults.xsrfCookieName = 'csrftoken';
    //$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
	$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.headers.common["X-CSRFToken"] = window.csrf_token;
 }]);
/*
 app.config(function ($routeProvider) {

 $routeProvider
 .when('/', {
 templateUrl: 'pages/home.html',
 controller: 'mainController'
 })
 .otherwise({
 redirectTo: '/'
 });
 });*/

