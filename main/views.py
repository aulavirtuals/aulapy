
from django.http import HttpResponse
# from django.http import response
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import JsonResponse
from django.core import serializers
import json
from models import Test
from django.db import connection
from security import MiddlewarePear as middle


# Create your views here.
def index(request):
    return render(request, 'index.html')
    # return  HttpResponse("hola mundo")

@csrf_exempt
def procedure(request,prm,id):
    m=middle
    t=m.finderSession()
    t.intelligentToken()
    c = connection.cursor()
    c.execute("BEGIN")
    if prm =="3" or prm=="4":
        datas = json.loads(request.body)
        name = ('',datas["nombre"])[datas["nombre"]!='']
        c.callproc("sp_test", [prm, name, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data =json.dumps(results)
        responses = HttpResponse( (data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses
    else:
        name=''
        c.callproc("sp_test", [prm, name, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data =json.dumps(results)
        responses = HttpResponse( (data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses


@csrf_exempt
def services(request, pr, id):
    stm = dml()
    if pr == "1":
        t = stm.read()

        r = []
        for x in t:
            d = {
                'name': x.nombre,
                'id': x.pk
            }
            r.append(d)
        response = HttpResponse(json.dumps(r), content_type='application/json; charset=utf-8;')
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
        # response["Access-Control-Max-Age"] = "1000"
        response["Access-Control-Allow-Headers"] = "*"
        return response

    else:
        if pr == "2":
            data = stm.create(request)
            responses = HttpResponse(json.dumps(data), content_type='application/json; charset=utf-8;')
            responses["Access-Control-Allow-Origin"] = "*"
            return responses
        else:
            if pr == "3":
                data = stm.update(request, id)
                responseu = HttpResponse(json.dumps(data), content_type='application/json; charset=utf-8;')
                responseu["Access-Control-Allow-Origin"] = "*"
                return responseu
            else:
                if pr == "4":
                    data = stm.delete(id)
                    responsed = HttpResponse(json.dumps(data), content_type='application/json; charset=utf-8;')
                    responsed["Access-Control-Allow-Origin"] = "*"
                    return responsed


@csrf_protect
def crud(request, pr, id):
    stm = dml()
    if pr == "1":
        t = stm.read()

        r = []
        for x in t:
            d = {
                'name': x.nombre,
                'id': x.pk
            }
            r.append(d)
        return HttpResponse(json.dumps(r), content_type='application/json; charset=utf-8;')
    else:
        if pr == "2":
            data = stm.create(request)

            return HttpResponse(json.dumps(data), content_type='application/json; charset=utf-8;')
        else:
            if pr == "3":
                data = stm.update(request, id)
                return HttpResponse(json.dumps(data), content_type='application/json; charset=utf-8;')
            else:
                if pr == "4":
                    data = stm.delete(id)
                    return HttpResponse(json.dumps(data), content_type='application/json; charset=utf-8;')


class dml:
    def read(self):
        t = Test.objects.all()
        return t
        # return HttpResponse(json.dumps(r), content_type='application/json; charset=utf-8;')

    def create(self, request):
        j = []
        # name = request.POST["nombre"]
        # name = request.POST.get('nombre')
        data = json.loads(request.body)
        name = data["nombre"]
        # name = request.data['nombre']
        message = "Se grabo el registro"
        c = Test(nombre=name)
        c.save()
        d = {
            "name": name,
            "message": message
        }
        j.append(d)
        return j

    def update(self, request, id):
        j = []
        # name = request.POST["nombre"]
        data = json.loads(request.body)
        name = data["nombre"]
        message = "Se actualizo el registro"
        c = Test.objects.get(pk=id)
        c.nombre = name
        c.save()
        d = {
            "message": message
        }
        j.append(d)
        return j

    def delete(self, id):
        j = []
        message = "Se elimino el registro"
        Test.objects.get(pk=id).delete()
        d = {
            "message": message
        }
        j.append(d)
        return j
        # return HttpResponse(json.dumps(j), content_type='application/json; charset=utf-8;')
