from __future__ import print_function

import datetime

# from django.contrib.auth import login
from django.contrib.auth.models import User
from django.core.signing import Signer
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
# from django.contrib.sessions.backends.db import SessionStore
from models import SessionTokens as sessionsx, LogCourse
from django.contrib.auth.backends import RemoteUserBackend
from django.contrib.sessions.backends.db import SessionStore
from security import MiddlewarePear as middle
from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login, logout
# from django.contrib.auth.hashers import check_password
# from django.http import request as rq
# from cookielib import CookieJar,CookiePolicy
# import requests
# from models import Member
from models import AuthUser,Member,TeacherStudent,Roles

import jwt
import base64
import json


# crud
# Create your views here.
def formLogin(request):
    formulario = AuthenticationForm()
    return render(request, 'login.html', {'formulario': formulario})


def login1(request):
    # formulario = AuthenticationForm()
    if request.method == 'POST':
        formulario = AuthenticationForm(request.POST)
        if formulario.is_valid:
            username = request.POST['username']
            password = request.POST['password']
            print('usuario', username)
            print('clave', password)
            user = authenticate(username=username, password=password)
            print('user', user)
            if user is not None:
                login(request, user)
                # Redirect to a success page.
                return HttpResponse("logged")
            else:
                return HttpResponse("error")


@csrf_exempt
def login(request):
    now = datetime.datetime.now()
    client_address = get_ip(request)
    print(client_address + str(now))
    credentials = []
    signer = Signer()
    inputs = json.loads(request.body)
    email = inputs["email"]
    passws = inputs["password"]
    try:
        remember = ('', inputs["remember"])[inputs["remember"] != '']
    except Exception:
        remember = ''

    ac = User.objects.get(email=email)
    if ac.check_password(passws):
        # function time
        # m = Member.objects.get(id_user=id)
        time = datetime.datetime.now() + datetime.timedelta(0, 10000)
        print('time count', time)
        if remember == False or remember == '':
            payload = {
                'username': ac.username,
                'email': ac.email,
                'staff': ac.is_staff
                ,
                'exp': datetime.datetime.now() + datetime.timedelta(seconds=100)
            }
        else:
            payload = {
                'username': ac.username,
                'email': ac.email,
                'staff': ac.is_staff
            }

        strs = ac.email + ac.password
        tks = base64.encodestring(strs)
        # tokens = signer.sign(tks)
        tokens = jwt.encode({'tks': tks}, 'SECRET')
        s = sessionsx(s_tokens=tokens, s_email=ac.email)
        s.save()
        print('tks', tokens)
        proccess = "login"
        l = LogCourse(email=email, proccess=proccess, created_at=now, ip=client_address)
        l.save()
        token1 = {'credentials': ac.username, 'email': ac.email, 'name': ac.username, 'auth': 'true'
            , 'token': jwt.encode(payload, 'SECRET')}
        credentials.append(token1)
        responses = HttpResponse(json.dumps(credentials), content_type='application/json;charset=utf-8')
        responses["Access-Control-Allow-Origin"] = "*"
        # responses.set_cookie('token',jwt.encode(payload, 'SECRET'))
        return responses
    else:
        dte = {
            "credentials": "null",
            "auth": "false",
            "token": None
        }
        credentials.append(dte)
        print(credentials)
        responses = HttpResponse(json.dumps(credentials), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses


@csrf_exempt
def server(request):
    responses = HttpResponse("http://localhost:8000/", content_type='text/plain;')
    responses["Access-Control-Allow-Origin"] = "*"
    return responses


@csrf_exempt
def reset(request):
    m = middle
    t = m.finderSession()
    # t.rolToken(4)
    credentials = []
    inputs = json.loads(request.body)
    email = inputs["email"]
    passws = inputs["password"]
    ac = User.objects.get(email=email)
    ac.set_password(passws)
    ac.save()
    now = datetime.datetime.now()
    client_address = get_ip(request)
    proccess = "cambio de clave"
    l = LogCourse(email=email, proccess=proccess, created_at=now, ip=client_address)
    l.save()
    d = {
        'credentials': ac.email,
        'auth': 'true'
    }
    credentials.append(d)
    responses = HttpResponse(json.dumps(credentials), content_type='application/json;charset=utf-8;')
    responses["Access-Control-Allow-Origin"] = "*"
    return responses


@csrf_exempt
def getTokens1(request):
    credentials = []
    inputs = json.loads(request.body)
    token = ('', inputs["tokens"])[inputs["tokens"] != '']
    email = inputs["email"]
    try:
        payload = jwt.decode(token, 'SECRET')
        request.user = User.objects.get(
            username=payload.get('username'),
            is_active=True
        )
        dt = {'tokens': token, 'username': request.user.username}
        credentials.append(dt)
        responses = HttpResponse(json.dumps(credentials), content_type='application/json;charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        # return request.user
        print(request.user)
        return responses
    except jwt.ExpiredSignature, jwt.DecodeError:
        return HttpResponse({'Error': "Token is invalid"}, status="403")
    except User.DoesNotExist:
        return HttpResponse({'Error': "Internal server error"}, status="500")


def getTokenAll(request):
    credentials = []
    now = datetime.datetime.now()
    try:
        # inputs = json.loads(request.body)
        # token = ('', inputs["tokens"])[inputs["tokens"] != '']
        # email = inputs["email"]
        try:
            s = sessionsx.objects.all()
            for i in s:
                tks = i.s_tokens
                id = i.id
                dt = {'tokens': tks, 'email': i.s_email, 'id': id}
                credentials.append(dt)
                responses = HttpResponse(json.dumps(credentials), content_type='application/json;charset=utf-8;')
                responses["Access-Control-Allow-Origin"] = "*"
                return responses
        except sessionsx.DoesNotExist:
            s = None
            client_address = get_ip(request)
            # proccess = "session unexpected"
            # l = LogCourse(email=email, proccess=proccess, created_at=now, ip=client_address)
            # l.save()
    except request.exceptions.HTTPError:
        d = {
            'tokens': None,
            'id': None
        }
        credentials.append(d)
        responses = HttpResponse(json.dumps(credentials), content_type='application/json;charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses


@csrf_exempt
def error(request):
    credentials = []
    inputs = json.loads(request.body)
    email = inputs["email"]
    now = datetime.datetime.now()
    client_address = get_ip(request)
    proccess = "session unexpected"
    l = LogCourse(email=email, proccess=proccess, created_at=now, ip=client_address)
    l.save()
    d = {
        'error': proccess
    }
    credentials.append(d)
    responses = HttpResponse(json.dumps(credentials), content_type='application/json;charset=utf-8')
    responses["Access-Control-Allow-Origin"] = "*"
    return responses


@csrf_exempt
def exit(request):
    token = []
    # input=json.loads(request.body)
    # email=input["email"]
    # tokensx=('',input["tokens"])[input["tokens"]!='']
    # s=sessions.objects.get(tokens=tokensx)
    # s.delete()
    payload = {
        'username': None,
        'email': None,
        'staff': None
    }
    jwt.encode(payload, 'SECRET')
    tokens = {"credentials": "null", "auth": "false", "email": None, "name": None, "token": None}
    token.append(tokens)
    print(token)
    responses = HttpResponse(json.dumps(token), content_type='application/json; charset=utf-8;')
    responses["Access-Control-Allow-Origin"] = "*"
    return responses


@csrf_exempt
def exits(request):
    token = []
    # middleware broken
    m = middle
    mi = m.finderSession()
    # mi.deleteSession()
    inputs = json.loads(request.body)
    email = inputs["email"]
    # tokensx=('',inputs["tokens"])[inputs["tokens"]!='']
    tokensx = inputs["tokens"]
    s = sessionsx.objects.filter(s_tokens=tokensx)
    s.delete()
    now = datetime.datetime.now()
    client_address = get_ip(request)
    proccess = "logout"
    l = LogCourse(email=email, proccess=proccess, created_at=now, ip=client_address)
    l.save()
    payload = {
        'username': None,
        'email': None,
        'staff': None,
        'exp': datetime.datetime.now()
    }
    tks = jwt.encode(payload, 'SECRET')
    tokens = {"credentials": "null", "auth": "false", "email": None, "name": None, "token": None, 'tokens': tks}
    token.append(tokens)
    print(token)
    responses = HttpResponse(json.dumps(token), content_type='application/json; charset=utf-8;')
    responses["Access-Control-Allow-Origin"] = "*"
    return responses


@csrf_exempt
def register(request):
    re = []
    inputs = json.loads(request.body)
    name = inputs["name"]
    email = inputs["email"]
    passw = inputs["password"]
    rol = inputs["rol"]
    user = User(username=name, email=email)
    user.set_password(passw)
    user.save()
    m=Member(id_user=user.pk,id_roles=rol)
    m.save()
    t=TeacherStudent(id_user=user.pk,id_rol=rol)
    t.save()
    now = datetime.datetime.now()
    client_address = get_ip(request)
    proccess = "register user"
    l = LogCourse(email=email, proccess=proccess, created_at=now, ip=client_address)
    l.save()
    d = {
        'message': 'se grabo el registro'
    }
    re.append(d)
    response = HttpResponse(json.dumps(re), content_type='application/json; charset=utf-8;')
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def roles(request):
    id=2
    id1=3
    credential=[]
    ids=[id,id1]
    r=Roles.objects.filter(id__in=ids)
    for v in r:
        d = {
            'id': v.id,
            'name': v.name
        }
        credential.append(d)
        response = HttpResponse(json.dumps(credential), content_type='application/json; charset=utf-8;')
        response["Access-Control-Allow-Origin"] = "*"
        return response



@csrf_exempt
def nick(request):
    try:
        re = []
        data = json.loads(request.body)
        email = data["email"]
        email2 = data["email2"]
        username = data["username"]
        user = AuthUser.objects.get(email=email)
        if user:
            user.username = username
            user.email2 = email2
            user.save()
            d = {
                'message': 'se grabo el registro'
            }
            re.append(d)
            response = HttpResponse(json.dumps(re), content_type='application/json; charset=utf-8;')
            response["Access-Control-Allow-Origin"] = "*"
            return response
    except User.DoesNotExist:
        return HttpResponse({'Error': "Internal server error"}, status="500")


def get_ip(request):
    """Returns the IP of the request, accounting for the possibility of being
    behind a proxy.
    """
    ip = request.META.get("HTTP_X_FORWARDED_FOR", None)
    if ip:
        # X_FORWARDED_FOR returns client1, proxy1, proxy2,...
        ip = ip.split(", ")[0]
    else:
        ip = request.META.get("REMOTE_ADDR", "")
    return ip
