"""aulaPy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
# from django.contrib import admin
# import django.views
# from main import views
from main import views as m
from room import views
from security import views as s
# from course import views as c
from authority import views as api

urlpatterns = {
    url(r'^main/', m.index, name='main'),
    url(r'^store/(?P<prm>\d+)/(?P<id>\d+)/$', m.procedure, name='store'),
    url(r'^crud/(?P<pr>\d+)/(?P<id>\d+)/$', m.crud, name='crud'),
    # url(r'^crud/(?P<pr>\d+)/(?P<id>\d+)/$', 'main.views.crud'),
    url(r'^services/(?P<pr>\d+)/(?P<id>\d+)/$', m.services, name='services'),
    # url(r'^login/', s.entry, name='login'),
    url(r'^api/login/', s.login, name='login'),
    url(r'^formLogin', s.formLogin, name='formLogin'),
    url(r'^login1/', s.login1, name='login1'),
    url(r'^api/tokens/', s.getTokens1, name='tokens'),
    url(r'^api/getTokens/', s.getTokenAll, name='getTokens'),
    url(r'^api/register/', views.register, name='register'),
    url(r'^api/change-password/', s.reset, name='change-password'),
    # url(r'^logout/',views.exit,name='logout')
    url(r'^api/logout/', s.exit, name='logout'),
    url(r'^api/change-nick/', s.nick, name='change-nick'),
    url(r'^api/server', s.server, name='server'),
    url(r'^api/triggersError/', s.error, name='triggersError'),
    url(r'^api/logouts/', s.exits, name='logouts'),
    # ----------module course-----------------
    # url(r'^course/(?P<prm>\d+)/(?P<id>\d+)/$', c.procedureCourse, name='course'),
    # url(r'^subject/(?P<prm>\d+)/(?P<id>\d+)/$', c.procedureSubject, name='subject'),
    # url(r'^cost/(?P<prm>\d+)/(?P<id>\d+)/$', c.procedureCost, name='cost'),
    # url(r'^enrolment/(?P<prm>\d+)/(?P<id>\d+)/$', c.procedureEnrolment, name='enrolment'),
    # url(r'^matter/(?P<prm>\d+)/(?P<id>\d+)/$', c.procedureMatterContent, name='matter')
    # ,
    # nueva implementacion llamada authority
    url(r'^api/home/(?P<prm>\d+)/(?P<id>\d+)/$', api.web1, name='home'),
    url(r'^api/roles/', s.roles, name='roles'),
    url(r'^api/(?P<module>\w+)/(?P<prm>\d+)/(?P<id>\d+)/$', api.web, name='api')
}
