from __future__ import print_function

from django.http import HttpResponse
# from django.http import response
from django.views.decorators.csrf import csrf_exempt
import json
from django.db import connection


@csrf_exempt
def procedureCourse1(request, prm, id):
    c = connection.cursor()
    c.execute("BEGIN")
    name = ''
    descrip = ''
    status = 0
    c.callproc("sp_course", [prm, name, descrip, status, id])
    results = c.fetchone()
    c.execute("COMMIT")
    c.close()
    data = json.dumps(results)
    responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
    responses["Access-Control-Allow-Origin"] = "*"
     # responses["Access-Control-Allow-Origin"] = "http://localhost:9000"
    return responses


@csrf_exempt
def procedureCourse(request, prm, id):
    c = connection.cursor()
    c.execute("BEGIN")
    if prm == "3" or prm == "4":
        datas = json.loads(request.body)
        name = ('', datas["name"])[datas["name"] != '']
        descrip = ('', datas["description"])[datas["description"] != '']
        status = ('', datas["status"])[datas["status"] != '']
        c.callproc("sp_course", [prm, name, descrip, status, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data = json.dumps(results)
        responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        # responses["Access-Control-Allow-Origin"] = "http://localhost:9000"
        return responses
    else:
        name = ''
        descrip = ''
        status = 0
        c.callproc("sp_course", [prm, name, descrip, status, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data = json.dumps(results)
        responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        # responses["Access-Control-Allow-Origin"] = "http://localhost:9000"
        return responses


@csrf_exempt
def procedureSubject(request, prm, id):
    c = connection.cursor()
    c.execute("BEGIN")
    if prm == "3" or prm == "4":
        datas = json.loads(request.body)
        idcourse = ('', datas["id_course"])[datas["id_course"] != '']
        name = ('', datas["name"])[datas["name"] != '']
        status = ('', datas["status"])[datas["status"] != '']
        c.callproc("sp_subject", [prm, idcourse, name, status, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data = json.dumps(results)
        responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses
    else:
        idcourse = 0
        name = ''
        status = 0
        c.callproc("sp_subject", [prm, idcourse, name, status, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data = json.dumps(results)
        responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses


@csrf_exempt
def procedureCost(request, prm, id):
    c = connection.cursor()
    c.execute("BEGIN")
    if prm == "3" or prm == "4":
        datas = json.loads(request.body)
        idcourse = ('', datas["id_course"])[datas["id_course"] != '']
        cost0 = ('', datas["cost_init"])[datas["cost_init"] != '']
        cost = ('', datas["cost"])[datas["cost"] != '']
        name = ('', datas["name"])[datas["name"] != '']
        descrip = ('', datas["description"])[datas["description"] != '']
        status = ('', datas["status"])[datas["status"] != '']
        idcourse = int(idcourse)
        cost0 = float(cost0)
        cost = float(cost)
        id = int(id)
        status = int(status)
        prm = int(prm)
        c.callproc("sp_cost_course", [prm, idcourse, cost0, cost, name, descrip, status, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data = json.dumps(results)
        responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses
    else:
        idcourse = 0
        cost0 = 0
        cost = 0
        name = ''
        descrip = ''
        status = 0
        c.callproc("sp_cost_course", [prm, idcourse, cost0, cost, name, descrip, status, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data = json.dumps(results)
        responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses


@csrf_exempt
def procedureEnrolment(request, prm, id):
    c = connection.cursor()
    c.execute("BEGIN")
    if prm == "3" or prm == "4":
        datas = json.loads(request.body)
        idcourse = ('', datas["id_course"])[datas["id_course"] != '']
        capacity = ('', datas["capacity"])[datas["capacity"] != '']
        idcourse = int(idcourse)
        capacity = int(capacity)
        c.callproc("sp_enrolment", [prm, idcourse, capacity, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data = json.dumps(results)
        responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses
    else:
        idcourse = 0
        capacity = 0
        c.callproc("sp_enrolment", [prm, idcourse, capacity, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data = json.dumps(results)
        responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses


@csrf_exempt
def procedureMatterContent(request, prm, id):
    c = connection.cursor()
    c.execute("BEGIN")
    if prm == "3" or prm == "4":
        datas = json.loads(request.body)
        pidsubject = ('', datas["id_subject"])[datas["id_subject"] != '']
        ptitle = ('', datas["title"])[datas["title"] != '']
        pstatus = ('', datas["status"])[datas["status"] != '']
        pcontent = ('', datas["content"])[datas["content"] != '']
        c.callproc("sp_matter_content", [prm, pidsubject, ptitle, pcontent, pstatus, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data = json.dumps(results)
        responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses
    else:
        pidsubject = 0
        ptitle = ''
        pcontent = ''
        pstatus = 0
        c.callproc("sp_matter_content", [prm, pidsubject, ptitle, pcontent, pstatus, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data = json.dumps(results)
        responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses


@csrf_exempt
def procedureSecurity(request, prm, id):
    c = connection.cursor()
    c.execute("BEGIN")
    if prm == "3":
        datas = json.loads(request.body)
        idroles = ('', datas["id_roles"])[datas["id_roles"] != '']
        iduser = ('', datas["id_user"])[datas["id_user"] != '']
        c.callproc("sp_security", [prm, idroles, iduser, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data = json.dumps(results)
        responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses
    else:
        idroles = 0
        iduser = 0
        c.callproc("sp_security", [prm, idroles, iduser, id])
        results = c.fetchone()
        c.execute("COMMIT")
        c.close()
        data = json.dumps(results)
        responses = HttpResponse((data), content_type='application/json; charset=utf-8;')
        responses["Access-Control-Allow-Origin"] = "*"
        return responses


'''
class finderSession:
    def sessionFind(self):
        s=Session.objects.all()
        for v in s:
            data=v.session_data
            key=v.session_key
            sub=SessionStore(session_key=key)
            return ((sub['token']))

    def deleteSession(self):
        s=self.sessionFind()
        sess=SessionStore()
        sess['token']=None
        sess.save()
        sess.clear()
        return "close session"
'''
'''
class JWT_AuthMiddleware(object):
    def _get_token(request=None):
        return request.META.get('HTTP_AUTHORIZATION') or request.GET.get('token')

    def process_request(self, request):
        token = self._get_token(request)
        try:
            payload = jwt.decode(token, 'SECRET')
            request.user = User.objects.get(
                username=payload.get('username'),
                pk=payload.get('id'),
                is_active=True
            )
            return request.user
        except jwt.ExpiredSignature, jwt.DecodeError:
            return HttpResponse({'Error': "Token is invalid"}, status="403")
        except User.DoesNotExist:
            return HttpResponse({'Error': "Internal server error"}, status="500")
'''
